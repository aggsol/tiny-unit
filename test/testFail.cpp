#include "tiny-unit.hpp"

#include <stdexcept>
#include <iostream>

/**
* Every unit test has to be derived from tiny::Unit
* This unit contains only failing tests.
*/
class TestFail : public tiny::Unit
{
public:
    TestFail()
    : Unit("Failing")
    {
        /**
        * Just register static or free functions. Every function is one
        * test case. Name them properly in context of their unit.
        */
        registerTest(TestFail::equalFail, "equalFail");
        registerTest(TestFail::equalStringFail, "equalStringFail");
        registerTest(TestFail::okFail, "okFail");
        registerTest(TestFail::throwException, "throwException");
        registerTest(TestFail::throwOther, "throwOther");
        registerTest(TestFail::wrongCatch, "wrongCatch");
        registerTest(TestFail::throwNothing, "throwNothing");

        /**
        * Wrong ways to register tests
        */
        try
        {
            registerTest(nullptr, "throws an exception"); // BAD
        }
        catch(...) {}

        try
        {
            registerTest(TestFail::wrongCatch, ""); // BAD
        }
        catch(...) {}
    }

    /**
    * Expect an exception that is never thrown.
    */
    static void throwNothing()
    {
        int i = 1;
        TINY_ASSERT_TRY();
            i += 1;
        TINY_ASSERT_CATCH(std::runtime_error);
    }

    /**
    * Wrong type of exception is thrown.
    */
    static void wrongCatch()
    {
        TINY_ASSERT_TRY();
            throw std::string("ERROR");
        TINY_ASSERT_CATCH(std::runtime_error);
    }

    static void throwOther()
    {
        throw std::string("ERROR");
    }

    static void throwException()
    {
        throw std::runtime_error("Generic error.");
    }

    static void okFail()
    {
        TINY_ASSERT_OK(3 == (4+5));
    }

    static void equalFail()
    {
        TINY_ASSERT_EQUAL('a', 'B');
    }

    static void equalStringFail()
    {
        TINY_ASSERT_EQUAL(std::string("Adam"), "Eve");
    }
};

TestFail testFail;
