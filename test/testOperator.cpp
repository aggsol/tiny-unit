#include "tiny-unit.hpp"

#include <iostream>
#include <string>

/**
 * This example shows how custom == operators are used for equality asserions
 */

class Blue;

class Reddish
{
public:
    explicit Reddish(const std::string& n)
    : name(n)
    {}

    friend bool operator==(const Reddish& lhs, const Blue& rhs);
    friend bool operator==(const Reddish& lhs, const Reddish& rhs);
    friend std::ostream& operator<<(std::ostream&, const Reddish&);

    std::string name;
};

std::ostream& operator<<(std::ostream& out, const Reddish& r)
{
    out << r.name;
    return out;
}

bool operator==(const Reddish& lhs, const Reddish& rhs)
{
    if(rhs.name == "red" || rhs.name == "crimson")
    {
        if(lhs.name == "red" || lhs.name == "crimson")
        {
            return true;
        }
    }
    return false;
}

class Blue
{
public:
    Blue() = default;

    friend bool operator==(const Reddish& lhs, const Blue& rhs);
    friend std::ostream& operator<<(std::ostream&, const Blue&);
};

bool operator==(const Reddish&, const Blue&)
{
    return false;
}


bool operator==(const Blue&, const Reddish&)
{
    return false;
}

class TestOperator : public tiny::Unit
{
public:
    TestOperator()
    : tiny::Unit("Equal Operator")
    {
        tiny::Unit::registerTest(&TestOperator::customOperator, "customOperator");
    }

    static void customOperator()
    {
        Reddish red("red");
        Reddish crimson("crimson");
        Reddish blood("blod");

        TINY_ASSERT_EQUAL(red, red);
        TINY_ASSERT_EQUAL(red, crimson);

        TINY_ASSERT_OK(red == crimson);
        TINY_ASSERT_OK(!(blood == crimson));

        //TINY_ASSERT_OK(red != crimson); // doesn't work != operator missing

        Blue blue;
        //TINY_ASSERT_EQUAL(red, blue); // fails because operator is missing
        TINY_ASSERT_OK( !(red == blue) );
        TINY_ASSERT_OK( !(blue == red) );
        TINY_ASSERT_OK( !(crimson == blue) );
        TINY_ASSERT_OK( !(blood == blue) );

        TINY_ASSERT_EQUAL(red, blood); // fails
    }

};

TestOperator testOperator;
