/**
* Required include for all unit tests.
*/
#include "tiny-unit.hpp"

/**
* Every unit test has to be derived from tiny::Unit
* Recommendation: Just implement one unit per file.
* Tip: Make the your test a friend class of the unit you are testing!
*/
class TestCaseSimple : public tiny::Unit
{
public:
    TestCaseSimple()
    /**
    * Properly name your unit. You can also use __FILE__
    */
    : tiny::Unit("Examples")
    {
        /**
        * Just register static or free functions. Every function is one
        * test case. Name them properly in context of their unit. The
        * test cases are run in order of registration.
        */
        tiny::Unit::registerTest(&TestCaseSimple::testEqual, "Equal");
        tiny::Unit::registerTest(&TestCaseSimple::testOk, "Assertions");
        tiny::Unit::registerTest(&TestCaseSimple::testException, "Exceptions");
    }

    /**
    * Test for equality for any types that can be tested for equality.
    * Custom types must provide the == operator.
    */
    static void testEqual()
    {
        TINY_ASSERT_EQUAL(1, 3-2);

        /**
        * Testing a char and int for equality is allowed.
        */
        TINY_ASSERT_EQUAL('a', 0x61);

        /**
        * This special case does NOT work, instead for string comparision use
        * std::string.
        */
        // TINY_ASSERT_EQUAL("Hello World!", "Hello World!"); BAD!
        TINY_ASSERT_EQUAL(std::string("Hello World!"), "Hello World!");
    }

    /**
    * Test any boolean expression if it is true.
    */
    static void testOk()
    {
        TINY_ASSERT_OK( 3 > 1 );
        TINY_ASSERT_OK( false == false );
        TINY_ASSERT_OK( true );
    }

    /**
    * Assert that your code throws a specified exception.
    */
    static void testException()
    {
        /**
        * Enclose the tested code with TINY_ASSERT_TRY() and TINY_ASSERT_CATHCH(typename).
        * The type name is the expected exception type.
        */
        TINY_ASSERT_TRY();
            throw int(2);
        TINY_ASSERT_CATCH( int );
    }
};

/**
* Instanciate your unit test.
*/
TestCaseSimple testCaseSimple;
