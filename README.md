## Tiny Unit v1.4.0
A tiny unit test framework for C++

### About
This is the most minimal unit test framework I can come up with. It supports assertions for boolean expressions, equality and exceptions. All units and their test cases are compiled into one binary and executed. You can use it as a drop-in or copy-pasta unit test framework. In any other case use something bigger.

## Examples
See `testCases.cpp` and `testFail.cpp` for detailed examples.

```cpp
/**
* Required include for all unit tests.
*/
#include "tiny-unit.hpp"

/**
* Every unit test has to be derived from tiny::Unit
* Recommendation: Just implement one unit per file.
* Tip: Make the your test a friend class of the unit you are testing!
*/
class TestCaseSimple : public tiny::Unit
{
public:
    TestCaseSimple()
    /**
    * Properly name your unit. You can also use __FILE__
    */
    : tiny::Unit("Examples")
    {
        /**
        * Just register static or free functions. Every function is one
        * test case. Name them properly in context of their unit. The
        * test cases are run in order of registration.
        */
        tiny::Unit::registerTest(&TestCaseSimple::testEqual, "Equal");
        tiny::Unit::registerTest(&TestCaseSimple::testOk, "Assertions");
        tiny::Unit::registerTest(&TestCaseSimple::testException, "Exceptions");
    }

    /**
    * Test for equality for any types that can be tested for equality.
    * Custom types must provide the == operator.
    */
    static void testEqual()
    {
        TINY_ASSERT_EQUAL(1, 3-2);

        /**
        * Testing a char and int for equality is allowed.
        */
        TINY_ASSERT_EQUAL('a', 0x61);

        /**
        * This special case does NOT work, instead for string comparision use
        * std::string.
        */
        // TINY_ASSERT_EQUAL("Hello World!", "Hello World!"); BAD!
        TINY_ASSERT_EQUAL(std::string("Hello World!"), "Hello World!");
    }

    /**
    * Test any boolean expression if it is true.
    */
    static void testOk()
    {
        TINY_ASSERT_OK( 3 > 1 );
        TINY_ASSERT_OK( false == false );
        TINY_ASSERT_OK( true );
    }

    /**
    * Assert that your code throws a specified exception.
    */
    static void testException()
    {
        /**
        * Enclose the tested code with TINY_ASSERT_TRY() and TINY_ASSERT_CATHCH(typename).
        * The type name is the expected exception type.
        */
        TINY_ASSERT_TRY();
            throw int(2);
        TINY_ASSERT_CATCH( int );
    }
};

/**
* Instanciate your unit test.
*/
TestCaseSimple testCaseSimple;

```

### Example Output
```sh
Unit Test: Examples
[ Ok ] Equal
[ Ok ] Assertions
[ Ok ] Exceptions
Unit Test: Failing
[FAIL] equalFail tiny-unit/test/testFail.cpp:82: Not equal. Expected=<B> Actual=<a>
[FAIL] equalStringFail tiny-unit/test/testFail.cpp:87: Not equal. Expected=<Eve> Actual=<Adam>
[FAIL] okFail tiny-unit/test/testFail.cpp:77: <3 == (4+5)> is false.
[FAIL] throwException  Unexpected exception: Generic error.
[FAIL] throwOther  Unexpected type thrown!
[FAIL] wrongCatch  Unexpected type thrown!
[FAIL] throwNothing tiny-unit/test/testFail.cpp:52: Expected exception std::runtime_error. Nothing caught.
Unit Test: Equal Operator
[FAIL] customOperator tiny-unit/test/testOperator.cpp:94: Not equal. Expected=<blod> Actual=<red>
1/3 unit tests passed.
```

## Usage
Just copy the `tiny-unit.hpp` and `tiny-unit.cpp` files in your project. The `main()` is already defined you just need to link your unit tests. The command line interface accepts a test name as optional parameter.

## Build and Test

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./tiny-unit
$ ./tiny-unit testEqual
```
The test runner returns 0 when no test failed, 1 otherwise. The tests in `testFail.cpp` are supposed to fail for demonstration.

## License
* tiny-unit is [Beerware](https://en.wikipedia.org/wiki/Beerware)
* Icon: Spoted flower icon by [Lorc](http://lorcblog.blogspot.com/) under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
